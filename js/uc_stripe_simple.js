//4242424242424242

//define a variable of global scope
var uc_stripe_simple_submit_buton;
Drupal.behaviors.uc_stripe_simple = {
	'attach': function(context, settings) {
	}
};

jQuery(document).ready(function() {
	
	//hide the stripe token input
	jQuery('.form-item-panes-payment-details-cc-stripetoken').css({'visibility':'hidden'});
	//alert('jqstripe: document ready fired');
	
	//hook the form-submit button to log the ID of the element generating it, since a form can have many submits
	jQuery(".form-submit").click(function(e, target) {
		uc_stripe_simple_submit_buton = e.currentTarget.id;
		console.log(e);
	});
	
	//we don't want the CCno, etc to persist between loads (in case of validation failure) because the name/address might change
	jQuery('#edit-panes-payment-details-cc-number').val('');
	jQuery('#edit-panes-payment-details-cc-cvv').val('');
	
	//hook the submit event
	jQuery('#uc-cart-checkout-form').submit(function() {
		if (uc_stripe_simple_submit_buton == 'edit-continue')
		{
			//intercept the review order event here and do the stripe stuff
			jQuery('#edit-continue').attr("disabled", "disabled");
			console.log('submit attempted');
			
			Stripe.createToken({
				number: jQuery('#edit-panes-payment-details-cc-number').val(),
				cvc: jQuery('#edit-panes-payment-details-cc-cvv').val(),
				exp_month: uc_stripe_simple_zeropad(jQuery('#edit-panes-payment-details-cc-exp-month').val()),
				exp_year: jQuery('#edit-panes-payment-details-cc-exp-year').val()
			}, uc_stripe_simple_stripe_response_handler);
			
			return false;
		}
	});
});

function uc_stripe_simple_stripe_response_handler(status, response) {
	console.log(status);
	console.log(response);
    if ((response.error) || (jQuery("#clientsidevalidation-uc-cart-checkout-form-errors").is(':visible') == true)) {
        // show the errors on the form
        jQuery("#uc_stripe_simple_payment_stripeerror").html(response.error.message).css({'visibility':'visible','display':'block'});
        jQuery("#edit-continue").removeAttr("disabled");
    } else {
		
        var form$ = jQuery("#uc-cart-checkout-form");
        // token contains id, last4, and card type
        var token = response['id'];
        // insert the token into the form so it gets submitted to the server

        //UC does an isnumeric check which cant be overridden. We can add 12 zeros and the last 4. Gets the job done.
		var ccno = jQuery('#edit-panes-payment-details-cc-number').val();
		ccno = ccno.substr(12,4);
		jQuery('#edit-panes-payment-details-cc-number').val("000000000000" + ccno);
		
		//set the stripe token before posting back
		jQuery('#edit-panes-payment-details-cc-stripetoken').val(token);
		
		//this seems to appease Drupal
		//UC is expecting an input called op, but it's not there when JS submits the form so we have to send it
		form$.append("<span style='display:none'><input type'hidden' value='Review order' name='op'></span>");
		
        // and submit
        form$.get(0).submit();
		
    }
}

function uc_stripe_simple_zeropad(h)
{
	h = (h < 10) ? ("0" + h) : h;
	return h;
}