<?php

/**
 * Implements hook_recurring_info().
 */
function uc_stripe_simple_process($order, &$fee) {
	_uc_stripe_simple_log("fired: uc_stripe_simple_process");
	/* reload the order, we stored the stripe token there 
	but the original one from UC was passed in */
	//$order_id = intval($order->order_id);
	$order_id = $order->order_id;
	//uc_stripe_dojo($order_id);
	unset($order);
	_uc_stripe_simple_log("order_id: " . $order_id);

	$order2 = uc_order_load($order_id,true);
	_uc_stripe_simple_log("order: " . _uc_stripe_simple_objecthell($order));
	_uc_stripe_simple_log("order2: " . _uc_stripe_simple_objecthell($order2));
	_uc_stripe_simple_log("order2 payment: " . var_export($order2->payment_details,true));
	_uc_stripe_simple_log("fee: " . _uc_stripe_simple_objecthell($fee));
	return false;
}
/*
function uc_stripe_dojo($order_id)
{
	$order2 = uc_order_load($order_id,true);
	_uc_stripe_simple_log("order2 dojo: " . _uc_stripe_simple_objecthell($order2));
	
}
*/
function uc_stripe_simple_renew($order, &$fee) {
	_uc_stripe_simple_log("fired: uc_stripe_simple_renew");
	_uc_stripe_simple_log("order: " . _uc_stripe_simple_objecthell($order));
	_uc_stripe_simple_log("fee: " . _uc_stripe_simple_objecthell($fee));
	return false;
}
function uc_stripe_simple_cancel($order, &$fee) {
	_uc_stripe_simple_log("fired: uc_stripe_simple_cancel");
	_uc_stripe_simple_log("order: " . _uc_stripe_simple_objecthell($order));
	_uc_stripe_simple_log("fee: " . _uc_stripe_simple_objecthell($fee));
	return false;
}
